package com.demo.app.filters;

import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;


@Provider
public class SecurityContextFilter implements ContainerRequestFilter{

	
	private static final Logger logger = LoggerFactory.getLogger(SecurityContextFilter.class);
	@Override
	public ContainerRequest filter(ContainerRequest containerRequest) {

		logger.info("Entering security filter");
		logger.info("Url",containerRequest.getRequestUri());
		
		return containerRequest;
	}

}
