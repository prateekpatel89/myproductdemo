package com.demo.app.resources;

import java.io.IOException;


import javax.ws.rs.GET;

import javax.ws.rs.Path;

import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.demo.app.model.Subscription;
import com.demo.app.model.SubscriptionBuilder;
import com.demo.app.model.SubscriptionEvent;
import com.demo.app.model.response.APIResponse;

import com.demo.app.services.SubscriptionService;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.basic.DefaultOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.signature.QueryStringSigningStrategy;


@Component
@Path("/subscription")
public class SubscriptionResource {
	
	private  SubscriptionService subscriptionService;
	
	private static final Logger logger = LoggerFactory.getLogger(SubscriptionResource.class);
	
	@Autowired
	public SubscriptionResource(SubscriptionService subscriptionService) {
		this.subscriptionService = subscriptionService;
	}

		@GET
	    @Produces(MediaType.APPLICATION_JSON)
		@Path("/create")
	    public Response createSubscription( @QueryParam("eventUrl") String eventUrl) 
	    {
			
			SubscriptionEvent subscriptionEvent;
			APIResponse response = new APIResponse();
	       try{
	    	   subscriptionEvent = fetchSubscriptionEvent(eventUrl);
	       }catch(Exception e){
	    	   logger.error("Get request to subscription event failed" + e);
	    	   response.setSuccess("false");
	    	   response.setErrorCode("INVALID_RESPONSE");
	    	   response.setMessage("Get request to subscription event failed");
	    	   return Response.status(200).entity(response).build();
	       }
	        SubscriptionBuilder subscriptionBuilder = new SubscriptionBuilder(subscriptionEvent);
	        Subscription subscription = subscriptionBuilder.build();
	     
	        Subscription result = subscriptionService.createSubscription(subscription);
	        if(result != null){
	        	response.setAccountIdentifier(result.getSubscriptionId());
	        	response.setSuccess("true");
	        	return Response.status(200).entity(response).build();
	          }
	        
	        response.setSuccess("false");
	        response.setMessage("Subscrition not created");
	        response.setErrorCode("INVALID_RESPONSE");
	        
	        return Response.status(200).entity(response).build();
	    }
		
		@GET 
		@Produces(MediaType.APPLICATION_JSON)
		@Path("/cancel")
	    public Response cancelSubscription(@QueryParam("eventUrl") String eventUrl) 
	    {
			SubscriptionEvent subscriptionEvent;
			APIResponse response = new APIResponse();
			try{
				subscriptionEvent = fetchSubscriptionEvent(eventUrl);
			}
			catch(Exception e){
				 logger.error("Get request to subscription event failed" + e);
				 response.setSuccess("false");
				 response.setErrorCode("INVALID_RESPONSE");
		    	 response.setMessage("Get request to subscription event failed");
		    	 return Response.status(200).entity(response).build();
			}
			int subscriptionId = subscriptionEvent.getPayload().getAccount().getAccountIdentifier();
			System.out.println(subscriptionEvent);
			try{
				if(subscriptionService.getSubscription(subscriptionId) != null){
					subscriptionService.cancelSubscription(subscriptionId);
				}
			}catch(RuntimeException e)
			{
				 logger.error("Failed to cancel subscription" + e);
				 response.setSuccess("false");
				 response.setErrorCode("INVALID_RESPONSE");
		    	 response.setMessage("Failed to cancel subscription");
		    	 return Response.status(200).entity(response).build();
			}
			
			response.setSuccess("true");
	        return Response.status(200).entity(response).build();
		  }
		
		
		private  SubscriptionEvent fetchSubscriptionEvent(String eventUrl) throws OAuthMessageSignerException, OAuthExpectationFailedException, OAuthCommunicationException, ClientProtocolException, IOException{
			
			HttpGet request;
			request = eventUrl(eventUrl);
			
			logger.info("eventUrl:"+ request);
	        HttpClient client = HttpClientBuilder.create().build();
	        
	        HttpResponse response = client.execute(request);
	        
			String orderEvent = EntityUtils.toString(response.getEntity());
			
			logger.info("OrderEvent:"+ orderEvent);
	       
			ObjectMapper mapper = new ObjectMapper();
			SubscriptionEvent subscriptionEvent = mapper.readValue(orderEvent,SubscriptionEvent.class);
			
			return subscriptionEvent;
			
		}
		
		
		private HttpGet eventUrl(String url) throws OAuthMessageSignerException, OAuthExpectationFailedException, OAuthCommunicationException{
			
			String signedUrl = sign(url);
			HttpGet request = new HttpGet(signedUrl);
			request.addHeader("accept", "application/json");
			return request;
			
		}
		
		private String sign(String url) throws OAuthMessageSignerException, OAuthExpectationFailedException, OAuthCommunicationException{
			
			OAuthConsumer consumer = new DefaultOAuthConsumer("my-new-product-136484", "Ce3zUSCFW3cEn8Pq");
			consumer.setSigningStrategy( new QueryStringSigningStrategy());
			String signedUrl = consumer.sign(url);
			return signedUrl;
			
		}
	    

}
