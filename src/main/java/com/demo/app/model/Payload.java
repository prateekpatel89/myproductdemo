package com.demo.app.model;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE,
                getterVisibility = JsonAutoDetect.Visibility.NONE,
                setterVisibility = JsonAutoDetect.Visibility.NONE,
                creatorVisibility = JsonAutoDetect.Visibility.NONE)
public class Payload {
	
	
	private Company company;
	
	private Order order;
	
	private Account account;

	@JsonProperty
	public Company getCompany() {
		return company;
	}
	@JsonProperty
	public void setCompany(Company company) {
		this.company = company;
	}
	@JsonProperty
	public Order getOrder() {
		return order;
	}
	@JsonProperty
	public void setOrder(Order order) {
		this.order = order;
	}
	@JsonProperty
	public Account getAccount() {
		return account;
	}
	@JsonProperty
	public void setAccount(Account account) {
		this.account = account;
	}
	@Override
	public String toString() {
		return "Payload [company=" + company + ", order=" + order + ", account=" + account + "]";
	}
	
	
	

}
