package com.demo.app.model;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE,
                getterVisibility = JsonAutoDetect.Visibility.NONE,
                setterVisibility = JsonAutoDetect.Visibility.NONE,
                creatorVisibility = JsonAutoDetect.Visibility.NONE)

public class User {
	
	private String email;

	private String firstName;
	
	private String lastName;
	
	private String language;
	
	private String openId;
	
	private String uuid;
	
	@JsonProperty
	public String getEmail() {
		return email;
	}
	
	@JsonProperty
	public void setEmail(String email) {
		this.email = email;
	}
	
	@JsonProperty
	public String getFirstName() {
		return firstName;
	}
	@JsonProperty
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	@JsonProperty
	public String getLastName() {
		return lastName;
	}
	@JsonProperty
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	@JsonProperty
	public String getLanguage() {
		return language;
	}
	 @JsonProperty
	public void setLanguage(String language) {
		this.language = language;
	}
	 @JsonProperty
	public String getOpenId() {
		return openId;
	}
	 @JsonProperty
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	 @JsonProperty
	public String getUuid() {
		return uuid;
	}
	 @JsonProperty
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	@Override
	public String toString() {
		return "SubscriptionCreator [email=" + email + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", language=" + language + ", openId=" + openId + ", uuid=" + uuid + "]";
	}

}
