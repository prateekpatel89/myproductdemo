package com.demo.app.model;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE,
                getterVisibility = JsonAutoDetect.Visibility.NONE,
                setterVisibility = JsonAutoDetect.Visibility.NONE,
                creatorVisibility = JsonAutoDetect.Visibility.NONE)

public class Marketplace {
	
	private String baseUrl;
	
	private String partner;
	
	@JsonProperty
	public String getBaseUrl() {
		return baseUrl;
	}
	
	@JsonProperty
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
	@JsonProperty
	public String getPartner() {
		return partner;
	}
	@JsonProperty
	public void setPartner(String partner) {
		this.partner = partner;
	}
	@Override
	public String toString() {
		return "MarketPlace [baseUrl=" + baseUrl + ", partner=" + partner + "]";
	}

}
