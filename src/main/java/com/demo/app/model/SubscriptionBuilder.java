package com.demo.app.model;

public class SubscriptionBuilder {
	
	private final SubscriptionEvent subscriptionEvent;
	
	public SubscriptionBuilder(SubscriptionEvent subscriptionEvent) {
		this.subscriptionEvent=subscriptionEvent;
		
	}

	public Subscription build(){
		
		return new Subscription(0,subscriptionEvent.getType(),
				subscriptionEvent.getUser().getFirstName(),
				subscriptionEvent.getUser().getLastName(),
				subscriptionEvent.getUser().getEmail(),
				subscriptionEvent.getPayload().getCompany().getName(),
				subscriptionEvent.getPayload().getOrder().getEditingCode(),
				subscriptionEvent.getPayload().getOrder().getPricingDuration()
				);
		
	}

}
