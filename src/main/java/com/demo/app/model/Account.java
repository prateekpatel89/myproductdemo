package com.demo.app.model;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE,
                getterVisibility = JsonAutoDetect.Visibility.NONE,
                setterVisibility = JsonAutoDetect.Visibility.NONE,
                creatorVisibility = JsonAutoDetect.Visibility.NONE)
public class Account {
	
	int accountIdentifier;
	String status;
	
	@JsonProperty
	public int getAccountIdentifier() {
		return accountIdentifier;
	}
	@JsonProperty
	public void setAccountIdentifier(int accountIdentifier) {
		this.accountIdentifier = accountIdentifier;
	}
	@JsonProperty
	public String getStatus() {
		return status;
	}
	@JsonProperty
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "Account [accountIdentifier=" + accountIdentifier + ", status=" + status + "]";
	}

}
