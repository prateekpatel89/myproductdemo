package com.demo.app.model;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE,
                getterVisibility = JsonAutoDetect.Visibility.NONE,
                setterVisibility = JsonAutoDetect.Visibility.NONE,
                creatorVisibility = JsonAutoDetect.Visibility.NONE)
public class Company {
	
	private String country;
	
	private String name;
	
	private String uuid;
	
	private String website;
	@JsonProperty
	public String getCountry() {
		return country;
	}
	@JsonProperty
	public void setCountry(String country) {
		this.country = country;
	}
	@JsonProperty
	public String getName() {
		return name;
	}
	@JsonProperty
	public void setName(String name) {
		this.name = name;
	}
	@JsonProperty
	public String getUuid() {
		return uuid;
	}
	@JsonProperty
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	@JsonProperty
	public String getWebsite() {
		return website;
	}
	@JsonProperty
	public void setWebsite(String website) {
		this.website = website;
	}
	@Override
	public String toString() {
		return "Company [country=" + country + ", name=" + name + ", uuid=" + uuid + ", website=" + website + "]";
	}
	
	
	
	
}
