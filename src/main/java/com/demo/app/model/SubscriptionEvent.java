package com.demo.app.model;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE,
                getterVisibility = JsonAutoDetect.Visibility.NONE,
                setterVisibility = JsonAutoDetect.Visibility.NONE,
                creatorVisibility = JsonAutoDetect.Visibility.NONE)
public class SubscriptionEvent {
	
	private String type;
	
	private Marketplace marketplace;
	
	private User user;
	


	private Payload payload;

	@JsonProperty
	public String getType() {
		return type;
	}
	
	@JsonProperty
	public void setType(String type) {
		this.type = type;
	}
	

	@JsonProperty
	public Marketplace getMarketPlace() {
		return marketplace;
	}

	@JsonProperty
	public void setMarketPlace(Marketplace marketplace) {
		this.marketplace = marketplace;
	}
	
	@JsonProperty("creator")
	public User getUser() {
		return user;
	}
	@JsonProperty("creator")
	public void setUser(User user) {
		this.user = user;
	}

	
	@JsonProperty
	public Payload getPayload() {
		return payload;
	}
	 @JsonProperty
	public void setPayload(Payload payload) {
		this.payload = payload;
	}

	@Override
	public String toString() {
		return "SubscriptionEvent [type=" + type + ", marketplace=" + marketplace + ", subscriptionCreator="
				+ user + ", payload=" + payload + "]";
	}

	
	 
	
	
	 

}
