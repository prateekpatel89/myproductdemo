package com.demo.app.model;

public class Subscription {
	
	private int subscriptionId;
	
	private final String type;
	
	private final String creatorFirstName;
	
	private final String creatorLastName;
	
	private final String creatorMail;
	
	private final String company;
	
	private final String editionCode;
	
	private final String pricingDuration;

	public int getSubscriptionId() {
		return subscriptionId;
	}

	public String getType() {
		return type;
	}

	public String getCreatorFirstName() {
		return creatorFirstName;
	}

	public String getCreatorLastName() {
		return creatorLastName;
	}

	public String getCreatorMail() {
		return creatorMail;
	}

	public String getCompany() {
		return company;
	}

	public String getEditionCode() {
		return editionCode;
	}

	public String getPricingDuration() {
		return pricingDuration;
	}

	public void setSubscriptionId(int subscriptionId) {
		this.subscriptionId = subscriptionId;
	}
	
	public Subscription(int subscriptionId, String type, String creatorFirstName, String creatorLastName,
			String creatorMail, String company, String editionCode, String pricingDuration) {
		super();
		this.subscriptionId = subscriptionId;
		this.type = type;
		this.creatorFirstName = creatorFirstName;
		this.creatorLastName = creatorLastName;
		this.creatorMail = creatorMail;
		this.company = company;
		this.editionCode = editionCode;
		this.pricingDuration = pricingDuration;
	}

	@Override
	public String toString() {
		return "Subscription [subscriptionId=" + subscriptionId + ", type=" + type + ", creatorFirstName="
				+ creatorFirstName + ", creatorLastName=" + creatorLastName + ", creatorMail=" + creatorMail
				+ ", company=" + company + ", editionCode=" + editionCode + ", pricingDuration=" + pricingDuration
				+ "]";
	}
	
	

}
