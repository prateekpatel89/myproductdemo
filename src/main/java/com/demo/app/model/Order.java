package com.demo.app.model;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE,
                getterVisibility = JsonAutoDetect.Visibility.NONE,
                setterVisibility = JsonAutoDetect.Visibility.NONE,
                creatorVisibility = JsonAutoDetect.Visibility.NONE)
public class Order {
	
	private String editionCode;
	
	private String pricingDuration;
	
	@JsonProperty
	public String getEditingCode() {
		return editionCode;
	}
	@JsonProperty
	public void setEditingCode(String editionCode) {
		this.editionCode = editionCode;
	}
	@JsonProperty
	public String getPricingDuration() {
		return pricingDuration;
	}
	@JsonProperty
	public void setPricingDuration(String pricingDuration) {
		this.pricingDuration = pricingDuration;
	}
	
	@Override
	public String toString() {
		return "Order [editionCode=" + editionCode + ", pricingDuration=" + pricingDuration + "]";
	}

}
