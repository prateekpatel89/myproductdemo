package com.demo.app.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.app.model.Subscription;
import com.demo.app.services.SubscriptionService;
import com.demo.app.services.dao.SubscriptionDao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class SubscriptionServiceImpl implements SubscriptionService{
	
	private static final Logger logger = LoggerFactory.getLogger(SubscriptionServiceImpl.class);

	private SubscriptionDao subscriptionDao;
	
	@Autowired
	public SubscriptionServiceImpl(SubscriptionDao subscriptionDao) {
		
		this.subscriptionDao = subscriptionDao;	
	}
	
	@Override
	public Subscription createSubscription(Subscription subscription) {
		
		logger.info("Subscription created"+ subscription);
		Subscription result = subscriptionDao.createSubscription(subscription);
		if(result != null)
		{
			return result;
		}
		else
		{
			logger.warn("Subscription not created");
		}
		
		return null;
	}

	@Override
	public void cancelSubscription(int subscriptionId) {
	
		logger.info("Cancelling subscription with subscriptionId :" +subscriptionId);
		subscriptionDao.cancelSubscription(subscriptionId);
	}

	@Override
	public Subscription getSubscription(int subscriptionId) {
		Subscription subscription =  subscriptionDao.getSubscription(subscriptionId);
		return subscription;
	}

}
