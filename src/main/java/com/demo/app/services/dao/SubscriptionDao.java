package com.demo.app.services.dao;

import com.demo.app.model.Subscription;

public interface SubscriptionDao {
	
	public Subscription createSubscription(Subscription subscription);

	public void cancelSubscription(int subscriptionId);
	
	public Subscription getSubscription(int subscriptionId);

}
