package com.demo.app.services.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;
import com.demo.app.model.Subscription;
import com.demo.app.services.dao.SubscriptionDao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/*
 Memory store used here needs to be changed , persistance layer with 
 hibernate needs to be implemented 
 */


@Component
public class SubscriptionDaoImpl implements SubscriptionDao{
	
	private static final Logger logger = LoggerFactory.getLogger(SubscriptionDaoImpl.class);
	
	private Map<Integer, Subscription> subscriptionStore = new HashMap<Integer, Subscription>();
	
	  
		  
	@Override
	public Subscription createSubscription(Subscription subscription) {
	
		subscription.setSubscriptionId(getMaxId()+1);
		 subscriptionStore.put(subscription.getSubscriptionId(), subscription);  
		  return subscription; 
	}

	@Override
	public void cancelSubscription(int subscriptionId) {
		subscriptionStore.remove(subscriptionId); 
	}
	
	

	@Override
	public Subscription getSubscription(int subscriptionId) {
		Subscription subscription = subscriptionStore.get(subscriptionId);
		return subscription;
	}  
	
	 public  int getMaxId()  
	 {   int max=0;  
		 for (int id:subscriptionStore.keySet()) {    
		  if(max<=id)  
		   max=id;  
		  
		 }    
		 return max;  
	 }



}
