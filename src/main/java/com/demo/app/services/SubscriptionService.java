package com.demo.app.services;

import com.demo.app.model.Subscription;

/**
 * Service for product subscription management
 *
 * 
 */
public interface SubscriptionService {
	
	public Subscription createSubscription(Subscription subscription);
	
	public void cancelSubscription(int subscriptionId);
	
	public Subscription getSubscription(int subscriptionId);
	

}
